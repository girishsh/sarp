<?php
/**
 * Created by PhpStorm.
 * User: Satish B. Sojitra
 * Date: 4/10/14
 * Time: 10:09 AM
 */
class AW_sarp_Block_Customer_Subscription_Changeperiod extends Mage_Core_Block_Template
{

    public function getSubscriptionType()
    {
        $periods=array();
        /* $write = Mage::getSingleton('core/resource')->getConnection('core_write');
         $result=$write->query("select * from aw_sarp_periods");
         while($row = $result->fetch())
         {
             $periods[]=$row['name'];
         }*/
        $result= Mage::getModel('sarp/period')->getCollection()->getData();
        foreach($result as $data)
        {
            $periods[$data['id']]=$data['name'];
        }
        return $periods;

    }
}