<?php
/**
 * Created by PhpStorm.
 * User: retailon
 * Date: 4/6/14
 * Time: 4:11 PM
 */

class AW_Sarp_Block_Customer_Subscription_Checkbc extends Mage_Core_Block_Template
{


    /**
     * Returns current customer
     * @return Mage_Customer_Model_Customer
     */
    public function getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    /**
     * Returns current order
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->getData('order')) {
            $this->setOrder($this->getSubscription()->getOrder());
        }
        return $this->getData('order');
    }



    /**
     * Returns current order
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (!$this->getData('quote')) {
            $this->setQuote($this->getSubscription()->getQuote());
        }
        return $this->getData('quote');
    }

    public function getPaymentBlock()
    {
        if (!$this->getOrder()->getPayment()) {
            throw new AW_Sarp_Exception("Can't get order for subscription #{$this->getSubscription()->getId()}. DB Corrupt?");
        }
        return $this->helper('payment')->getInfoBlock($this->getOrder()->getPayment());
    }

    public function getSubscriptionStatusLabel()
    {
        return Mage::getModel('sarp/source_subscription_status')->getLabel($this->getSubscription()->getStatus());
    }

    public function getCurrentDate() {
        return date('Y-m-d');
    }

    public function getPeriodGone() {
        $subId = $this->getRequest()->getParam('id');
        $_resource = Mage::getSingleton('core/resource');
        $_seqTable = $_resource->getTableName('sarp/sequence');
        $_subTable = $_resource->getTableName('sarp/subscription');
        $connectionRead = $_resource->getConnection('core_read');
        $connectionWrite = $_resource->getConnection('core_write');
        $periodType = $connectionRead->fetchOne("SELECT period_type from {$_subTable} WHERE id = {$subId}");
        $periodDetails = $connectionRead->fetchAll("SELECT name, period_type, period_value, expire_type, expire_value from aw_sarp_periods WHERE id = {$periodType}");
        $noofseq = $connectionRead->fetchAll("SELECT sq.date, sq.status from {$_seqTable} as sq WHERE subscription_id = {$subId}");
        $count = 0;
        foreach($noofseq as $r) {
            if($r['status'] != 'pending'){
                $count = $count + 1;
            }
        }
        return $count;
    }

}