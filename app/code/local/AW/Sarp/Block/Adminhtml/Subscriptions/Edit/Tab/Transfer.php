<?php
/**
 * Created by PhpStorm.
 * User: Girish
 * Date: 12/5/14
 * Time: 3:13 PM
 */

 class AW_Sarp_Block_Adminhtml_Subscriptions_Edit_Tab_Transfer extends Mage_Core_Block_Template {

     public function _construct() {
         parent::_construct();
         $this->setTemplate('aw_sarp/subscription/transfer/transfer.phtml');
     }
 }