<?php
/**
* aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * aheadWorks does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * aheadWorks does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Sarp
 * @version    1.7.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE-COMMUNITY.txt
 */

class AW_Sarp_CustomerController extends Mage_Core_Controller_Front_Action
{

    /*
      *
      * check that current subscription belong current customer
      *
      */
	  
    public function removeAction()
    {
        $itemId = $this->getRequest()->getParam('itemId');
        $productId = $this->getRequest()->getParam('productId');
        $subscriptionId = $this->getRequest()->getParam('sId');

        echo $itemId.' ';
        echo $productId.' ';
        echo $subscriptionId.' ';
        $price = Mage::getModel('sales/order_item')->load($itemId)->getPrice();


        $orders = Mage::getModel('sarp/subscription_item')->getCollection()->getData();
        print_r($orders);

        foreach($orders as $items)
        {
            if($itemId ==  $items['primary_order_item_id'])
            {
                $orderId = $items['primary_order_id'];
            }
        }

        echo $orderId."hello\n";
        echo $price;
        $order_collection = Mage::getModel('sales/order')->load($orderId);

        $base_grand_total = $order_collection->getBaseGrandTotal() - $price;
        $base_subtotal = $order_collection->getBaseSubtotal() - $price;
        $grand_total = $order_collection->getGrandTotal() - $price;
        $subtotal = $order_collection->getSubtotal() - $price;

        echo $base_grand_total;
        echo $grand_total;
        echo $subtotal;

        $order_collection->setBaseGrandTotal($base_grand_total)->save();
        $order_collection->setBaseSubTotal($base_subtotal)->save();
        $order_collection->setGrandTotal($grand_total)->save();
        $order_collection->setSubtotal($subtotal)->save();

        $last_order_amount_record = Mage::getModel('sarp/subscription')->load($subscriptionId);

        $last_order_amount = $last_order_amount_record->getFlatLastOrderAmount() - $price;

        $last_order_amount_record->setFlatLastOrderAmount($last_order_amount)->save();
       /* $del_item = Mage::getModel('sarp/subscription_item')->load($itemId);
        $del_order= Mage::getModel('sales/order_item')->load($itemId);

        $del_item->delete();
        $del_order->delete();*/

    //    $where = "primary_order_item_id = "+$itemId;
         $write = Mage::getSingleton('core/resource')->getConnection('core_write');
         $write->query("delete from aw_sarp_subscription_items where primary_order_item_id =".$itemId);

        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $write->query("delete from sales_flat_order_item where item_id =".$itemId);

        Mage::getSingleton('customer/session')->addSuccess("Product Removed Successfully.");
        $this->_redirect('subscriptions/customer');
        return;


    }

    public function _check()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            $model = Mage::getModel('sarp/subscription')->load($id);
            if (Mage::getModel('customer/session')->getId() != $model->getCustomerId()) {
                Mage::getSingleton('core/session')->addError(Mage::helper('sarp')->__('This subscription isn\'t belong this customer!'));
                return false;
            }
            else
                return true;
        }
    }

    public function indexAction()
    {

        if (!Mage::getSingleton('customer/session')->getCustomerId()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return;
        }

        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('sarp/customer');
        }
        if ($block = $this->getLayout()->getBlock('sarp_subscription_list')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }

        $this->renderLayout();
    }

    /**
     * Change period frequency
     * @return
     */

    public function changeperiodAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Change the Shippment date - Girish
     * */

    public function changeshipdateAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function updateshipdateAction() {

        $shipDate =  $this->getRequest()->getParam("shipdate");
        $subId = $this->getRequest()->getParam('id');
        $_resource = Mage::getSingleton('core/resource');
        $_tableName = $_resource->getTableName('sarp/sequence');
        $connectionRead = $_resource->getConnection('core_read');
        $connectionWrite = $_resource->getConnection('core_write');
        if($this->getRequest()->getParam('perm') != 1) {
            $idMax = $connectionRead->fetchCol("SELECT id from {$_tableName} WHERE subscription_id = {$subId} AND {$_tableName}.date = (SELECT MIN({$_tableName}.date) from {$_tableName} WHERE subscription_id = {$subId}) and status = 'pending'");
            $sql = "UPDATE {$_tableName} SET {$_tableName}.date = '{$shipDate}' WHERE id = {$idMax[0]} and subscription_id = {$subId}";
            $connectionWrite->query($sql);
            $message = $this->__('Subscription shipment date changed successfully!');
            Mage::getSingleton('core/session')->addSuccess($message);
            $this->_redirect('subscriptions/customer');
            return;
        } else {
            $_periodTable = $_resource->getTableName('sarp/period');
            $_subsTable = $_resource->getTableName('sarp/subscription');
            $periodParams = $connectionRead->fetchAll("SELECT s.id as subid, p.period_type, p.period_value FROM {$_periodTable} AS p, {$_subsTable} AS s WHERE p.id = s.period_type and s.id = {$subId}");
            $sequenceDates = $connectionRead->fetchAll("SELECT id, date FROM {$_tableName} WHERE status = 'pending' and subscription_id = {$subId} ORDER BY date");
            if($periodParams[0]['period_type'] == "month") {
                $tempDate = $shipDate;
                foreach($sequenceDates as $s) {
                    $sql = "UPDATE {$_tableName} SET {$_tableName}.date = '{$tempDate}' WHERE id = {$s['id']} and subscription_id = {$subId}";
                    $connectionWrite->query($sql);
                    $dArr = explode("-", $tempDate);
                    $dArr[1] = $dArr[1] + $periodParams[0]['period_value'];
                    if($dArr[1] > 12){
                        $dArr[1] = $dArr[1]%12;
                        $dArr[0] = $dArr[0] + 1;
                    }
                    $tempDate = implode("-", $dArr);
                }
            } elseif($periodParams[0]['period_type'] == "year") {
                $tempDate = $shipDate;
                foreach($sequenceDates as $s) {
                    $sql = "UPDATE {$_tableName} SET {$_tableName}.date = '{$tempDate}' WHERE id = {$s['id']} and subscription_id = {$subId}";
                    $connectionWrite->query($sql);
                    $dArr = explode("-", $tempDate);
                    $dArr[0] = $dArr[0] + $periodParams[0]['period_value'];
                    $tempDate = implode("-", $dArr);
                }
            } elseif($periodParams[0]['period_type'] == "day") {
                $tempDate = $shipDate;
                foreach($sequenceDates as $s) {
                    $sql = "UPDATE {$_tableName} SET {$_tableName}.date = '{$tempDate}' WHERE id = {$s['id']} and subscription_id = {$subId}";
                    $connectionWrite->query($sql);
                    $days = $periodParams[0]['period_value'];
                    $tempDate = date('Y-m-d', strtotime($tempDate. ' + '.$days.' days'));
                }
            } elseif($periodParams[0]['period_type'] == "week") {
                $tempDate = $shipDate;
                foreach($sequenceDates as $s) {
                    $sql = "UPDATE {$_tableName} SET {$_tableName}.date = '{$tempDate}' WHERE id = {$s['id']} and subscription_id = {$subId}";
                    $connectionWrite->query($sql);
                    $days = $periodParams[0]['period_value'] * 7;
                    $tempDate = date('Y-m-d', strtotime($tempDate. ' + '.$days.' days'));
                }
            }
            $message = $this->__('Subscription shipment date changed successfully!');
            Mage::getSingleton('core/session')->addSuccess($message);
            $this->_redirect('subscriptions/customer');
            return;
        }
    }

    public function pauseAction() {
        $subId = $this->getRequest()->getParam('id');
        $_resource = Mage::getSingleton('core/resource');
        $_seqTable = $_resource->getTableName('sarp/sequence');
        $_subTable = $_resource->getTableName('sarp/subscription');
        $connectionRead = $_resource->getConnection('core_read');
        $connectionWrite = $_resource->getConnection('core_write');
        $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
        if ($model->getId()) {
            $model->setStatus(AW_Sarp_Model_Subscription::STATUS_SUSPENDED_BY_CUSTOMER)->save();
        } else {
            throw new AW_Sarp_Exception("Subscription not found!");
        }
        $sequenceDates = $connectionRead->fetchAll("SELECT id, date FROM {$_seqTable} WHERE status = 'pending' and subscription_id = {$subId} ORDER BY date");
        foreach($sequenceDates as $s) {
            $sql = "UPDATE {$_seqTable} SET status = 'paused' WHERE id = {$s['id']} and subscription_id = {$subId}";
            $connectionWrite->query($sql);
        }
        Mage::getSingleton('core/session')->addSuccess('You have successfully paused subscription');
        $this->_redirectReferer();
    }

    public function updateAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
        $model->setData('period_type',$this->getRequest()->getParam('period'))->save();

        $message = $this->__('Subscription frequency changed successfully!');
        Mage::getSingleton('core/session')->addSuccess($message);

        $this->_redirect('subscriptions/customer');
        return;
    }

    public function historyAction()
    {
        if (!Mage::getSingleton('customer/session')->getCustomerId()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $Subscription = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
        if ($block = $this->getLayout()->getBlock('sarp_subscription_payments_history')) {
            $block
                    ->setRefererUrl($this->_getRefererUrl())
                    ->setSubscription($Subscription);
        }
        if ($block = $this->getLayout()->getBlock('sarp_subscription_payments_pending')) {
            $block
                    ->setRefererUrl($this->_getRefererUrl())
                    ->setSubscription($Subscription);
        }

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('sarp/customer');
        }

        $this->renderLayout();
    }

    public function viewAction()
    {
        if (!Mage::getSingleton('customer/session')->getCustomerId()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        ;
        if ($block = $this->getLayout()->getBlock('sarp_subscription_summary')) {
            $block
                    ->setRefererUrl($this->_getRefererUrl())
                    ->setSubscription(Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id')));
        }

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('sarp/customer');
        }

        $this->renderLayout();
    }



    public function changeAction()
    {
        if (!Mage::getSingleton('customer/session')->getCustomerId()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return;
        }
        $subscription = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
        if (
            is_null($subscription->getCustomerId()) ||
            Mage::getSingleton('customer/session')->getCustomerId() != $subscription->getCustomerId()
        ) {
            $this->_redirect('subscriptions/customer');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($block = $this->getLayout()->getBlock('sarp_subscription_edit')) {
            $block
                    ->setRefererUrl($this->_getRefererUrl())
                    ->setSubscription(Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id')))
                    ->setSection($this->getRequest()->getParam('section'));
        }
        $this->renderLayout();
    }

    /**
     * Saves address
     * @return
     */
    public function saveAction()
    {
        try {
            $model = Mage::getModel('sarp/order_section')
                    ->setSubscription(Mage::getSingleton('sarp/subscription')->load($this->getRequest()->getParam('id')))
                    ->setType($this->getRequest()->getParam('section'))
                    ->setDataToSave($this->getRequest()->getPost())
                    ->save();
            Mage::getSingleton('customer/session')->addSuccess(Mage::helper('sarp')->__('Subscription has been successfully saved'));

            $this->_redirect('sarp/customer/view', array('id' => $this->getRequest()->getParam('id')));
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
            Mage::getSingleton('customer/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }
    }

    /**
     * Customer cancels subscription
     * @return
     */
    public function cancelAction()
    {
        try {
            $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
            if ($model->getId() && Mage::getModel('customer/session')->getId() == $model->getCustomerId()) {
                $model->cancel()->save();
            } else {
                throw new AW_Sarp_Exception("Subscription not found!");
            }
            Mage::getSingleton('core/session')->addSuccess('You have successfully canceled subscription');
            $this->_redirectReferer();
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
            Mage::getSingleton('core/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }
    }

    /**
     * Customer suspends subscription
     * @return
     */
    public function suspendAction()
    {
        if (!$this->_check())
            return $this->_redirect('/');

        try {
            $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
            if ($model->getId()) {
                $model->setStatus(AW_Sarp_Model_Subscription::STATUS_SUSPENDED_BY_CUSTOMER)->save();
            } else {
                throw new AW_Sarp_Exception("Subscription not found!");
            }
            Mage::getSingleton('core/session')->addSuccess('You have successfully suspended subscription');
            $this->_redirectReferer();
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
            Mage::getSingleton('core/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }
    }

    /**
     * Customer re-activates subscription
     * @return
     */
    public function activateAction()
    {
        if (!$this->_check())
            return $this->_redirect('/');

        try {
            $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
            if ($model->getId() && ($model->getStatus() == AW_Sarp_Model_Subscription::STATUS_SUSPENDED_BY_CUSTOMER)) {
                $model->setStatus(AW_Sarp_Model_Subscription::STATUS_ENABLED)->save();
            } else {
                throw new AW_Sarp_Exception("Activation of subscription is no allowed");
            }
            Mage::getSingleton('customer/session')->addSuccess('You have successfully activated subscription');
            $this->_redirectReferer();
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
            Mage::getSingleton('customer/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }
    }

    public function prolongAction()
    {

        if (!$this->_check())
            return $this->_redirect('/');

        $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
        $orderId = $model->getOrder()->getId();
        $order = $model->getOrder();

        $cart = Mage::getSingleton('checkout/cart');
        $cartTruncated = false;
        /* @var $cart Mage_Checkout_Model_Cart */

        $items = $order->getItemsCollection();
        foreach ($items as $item) {
            try {
                /* Checking for subscription type to prolong */
                if (!Mage::helper('sarp')->isSubscriptionType($item)) continue;

                /* Check if order item and one of subscription items match */
                $flag = false;
                foreach ($model->getItems() as $subscriptionItem)
                    if ($item->getId() == $subscriptionItem->getOrderItem()->getId()) {
                        $flag = true;
                        break;
                    }
                if (!$flag) continue;

                $iBR = $item->getProductOptionByCode('info_buyRequest');

                $newDate = new Zend_Date($iBR['aw_sarp_subscription_start'], Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT));
                $newDate = $model->getNextSubscriptionEventDate($model->getDateExpire());

                if ($newDate->compare(new Zend_Date) == -1 || ($model->getStatus() == AW_Sarp_Model_Subscription::STATUS_CANCELED)) {
                    $newDate = new Zend_Date;
                }
                $iBR['aw_sarp_subscription_start'] = $newDate->toString(Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT));
                $item->setProductOptions(array('info_buyRequest' => $iBR));

                $cart->addOrderItem($item);

            } catch (Mage_Core_Exception $e) {
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                }
                else {
                    Mage::getSingleton('checkout/session')->addError($e->getMessage());
                }
                $this->_redirectReferer();
            } catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addException($e,
                                                                     Mage::helper('checkout')->__('Can not add item to shopping cart')
                );
                $this->_redirect('checkout/cart');
            }
        }

        $cart->save();
        $this->_redirect('checkout/cart');

    }

    public function reorderAction()
    {
        $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
        $orderId = $model->getOrder()->getId();
        $order = $model->getOrder();

        $cart = Mage::getSingleton('checkout/cart');
        $cartTruncated = false;
        /* @var $cart Mage_Checkout_Model_Cart */

        $items = $order->getItemsCollection();
        foreach ($items as $item) {
            try {
                $iBR = $item->getProductOptionByCode('info_buyRequest');

                //$newDate = new Zend_Date($iBR['aw_sarp_subscription_start'], Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT));
                if ($date = $model->getLastPaidDate()) {

                } else {
                    $date = $model->getDateStart();
                }

                $newDate = $model->getNextSubscriptionEventDate($date);

                if ($newDate->compare(new Zend_Date) == -1 || ($model->getStatus() == AW_Sarp_Model_Subscription::STATUS_CANCELED)) {
                    $newDate = new Zend_Date;
                }
                $iBR['aw_sarp_subscription_start'] = $newDate->toString(Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT));
                $iBR['aw_sarp_order_id'] = $orderId;
                $item->setProductOptions(array('info_buyRequest' => $iBR));

                $cart->addOrderItem($item);
                $model->setStatus(AW_Sarp_Model_Subscription::STATUS_CANCELED)->save();
            } catch (Mage_Core_Exception $e) {
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                }
                else {
                    Mage::getSingleton('checkout/session')->addError($e->getMessage());
                }
                $this->_redirectReferer();
            } catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addException($e,
                                                                     Mage::helper('checkout')->__('Can not add item to shopping cart')
                );
                $this->_redirect('checkout/cart');
            }
        }

        $cart->save();
        $this->_redirect('checkout/cart');

    }

    public function addproductAction() {
        if (!Mage::getSingleton('customer/session')->getCustomerId()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return;
        }

        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('sarp/customer');
        }
        if ($block = $this->getLayout()->getBlock('sarp_subscription_addproduct')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }

        $this->renderLayout();
    }

    public function addAction() {
        $subId = $this->getRequest()->getParam('sid');
        $proId = $this->getRequest()->getParam('pid');
        $_resource = Mage::getSingleton('core/resource');
        $_seqTable = $_resource->getTableName('sarp/sequence');
        $_subTable = $_resource->getTableName('sarp/subscription');
        $connectionRead = $_resource->getConnection('core_read');
        $connectionWrite = $_resource->getConnection('core_write');
        $orderId = $connectionRead->fetchOne("SELECT primary_order_id from aw_sarp_subscription_items WHERE subscription_id = {$subId}");
        $product = Mage::getModel('catalog/product')->load($proId);
        $qty = 1;
        $order = Mage::getModel('sales/order')->load($orderId);
        $quote = Mage::getModel('sales/quote')->getCollection()->addFieldToFilter("entity_id", $order->getQuoteId())->getFirstItem();
        if (!$quote->getId()) {
            $quote = Mage::getModel('sales/convert_order')
                ->toQuote($order)
                ->setIsActive(false)
                ->setReservedOrderId($order->getIncrementId())
                ->save();
        }
        $quoteItem = Mage::getModel('sales/quote_item')
            ->setProduct($product)
            ->setQuote($quote)
            ->setQty($qty)
            ->save();
        $orderItem = Mage::getModel('sales/convert_quote')
            ->itemToOrderItem($quoteItem)
            ->setOrderID($order->getId())
            ->save($orderId);
        echo "Order placed";
        echo exit;


        //echo $orderId;
        //echo $subId."<br>".$proId."<br>".$orderId;
        //echo exit;
        /*$model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('sid'));
        if ($model->getId()) {
            $model->setStatus(AW_Sarp_Model_Subscription::STATUS_SUSPENDED_BY_CUSTOMER)->save();
        } else {
            throw new AW_Sarp_Exception("Subscription not found!");
        }
        $sequenceDates = $connectionRead->fetchAll("SELECT id, date FROM {$_seqTable} WHERE status = 'pending' and subscription_id = {$subId} ORDER BY date");
        foreach($sequenceDates as $s) {
            $sql = "UPDATE {$_seqTable} SET status = 'paused' WHERE id = {$s['id']} and subscription_id = {$subId}";
            $connectionWrite->query($sql);
        }
        Mage::getSingleton('core/session')->addSuccess('You have successfully paused subscription');
        $this->_redirectReferer();
        */
        //echo $subId."<br>".$proId;
        //echo exit;
    }

    public function checkbcAction() {
        if (!Mage::getSingleton('customer/session')->getCustomerId()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return;
        }

        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
            $navigationBlock->setActive('sarp/customer');
        }
        if ($block = $this->getLayout()->getBlock('sarp_subscription_addproduct')) {
            $block->setRefererUrl($this->_getRefererUrl());
        }

        $this->renderLayout();
    }

    public function continueAction() {
        $subId = $this->getRequest()->getParam('id');
        $_resource = Mage::getSingleton('core/resource');
        $_seqTable = $_resource->getTableName('sarp/sequence');
        $_subTable = $_resource->getTableName('sarp/subscription');
        $connectionRead = $_resource->getConnection('core_read');
        $connectionWrite = $_resource->getConnection('core_write');
        if (!$this->_check())
            return $this->_redirect('/');

        try {
            $model = Mage::getModel('sarp/subscription')->load($this->getRequest()->getParam('id'));
            if ($model->getId() && ($model->getStatus() == AW_Sarp_Model_Subscription::STATUS_SUSPENDED_BY_CUSTOMER)) {
                $model->setStatus(AW_Sarp_Model_Subscription::STATUS_ENABLED)->save();
            } else {
                throw new AW_Sarp_Exception("Activation of subscription is no allowed");
            }
            $sequenceDates = $connectionRead->fetchAll("SELECT id, date FROM {$_seqTable} WHERE status = 'paused' and subscription_id = {$subId} ORDER BY date");
            foreach($sequenceDates as $s) {
                $sql = "UPDATE {$_seqTable} SET status = 'pending' WHERE id = {$s['id']} and subscription_id = {$subId}";
                $connectionWrite->query($sql);
            }
            Mage::getSingleton('customer/session')->addSuccess('You have successfully continued subscription');
            $this->_redirectReferer();
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
            Mage::getSingleton('customer/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }
    }
}
